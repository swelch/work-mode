#!/usr/bin/python

import psutil
import time
from subprocess import Popen

def find_procs_by_name(name):
    "Return a list of processes matching 'name'."
    ls = []
    for p in psutil.process_iter(['name']):
        if p.info['name'] == name:
            ls.append(p)
    return ls

## Define my work applications in a dictionary
# App with process names for searching
work_apps = { 'chromium': 'chromium-browser',
              'RC_electron': 'RingCentral',
              'thunderbird': 'thunderbird',
              'slack': 'slack',
              'calendar': 'minetime'
            }

# App with how to start it from CLI
starting_apps = {   'chromium': 'chromium-browser',
                    'RC_electron': '~/Projects/ringcentral-electron-app/RingCentral-linux-x64/RingCentral',
                    'thunderbird': 'thunderbird',
                    'slack': 'flatpak run com.slack.Slack',
                    'calendar': '/opt/MineTime/minetime %U'

                }

# Sleep for a few seconds to let the external display connect.
time.sleep(15)

# Loop through work apps to see if they're running and start them if not
for app, app_ps in work_apps.items():
    app_search = find_procs_by_name(app_ps)
    if app_search:
        pass
    else:
        #print(starting_apps[app])
        start_app = Popen(starting_apps[app], shell=True)
        time.sleep(.5)
